# Set the base image
FROM centos
# Dockerfile author / maintainer 
MAINTAINER Bob Ho <bob.ho@appdynamics.com> 

USER root

# Pre-requisites for AppDynamics
RUN yum update -y && yum install -y wget && yum install -y libaio && yum install -y net-tools

# Authenticate in AppDynamics
WORKDIR /root
COPY auth.properties .
run cat auth.properties | xargs | sed -e 's/ /\&/g' | head -c -1 > auth.data
run wget --save-cookies cookies.txt --post-file auth.data --no-check-certificate https://login.appdynamics.com/sso/login/

# Download Controller
run wget --load-cookies cookies.txt https://download.appdynamics.com/download/prox/download-file/controller/4.3.4.3/controller_64bit_linux-4.3.4.3.sh -O controller.sh && chmod 755 controller.sh

#Transfer AppDynamics Response file
copy setup.varfile .

# Install AppDynamics Controller
run ./controller.sh -q -varfile setup.varfile

# Install AppDynamics License
copy license.lic /opt/AppDynamics/Controller/

# Expose ports
EXPOSE 8090

# Set working directory
WORKDIR /opt/AppDynamics/Controller/
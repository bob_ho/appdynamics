# AppDynamics Controller v 4.3.4.2 Docker Image
  
Repo contains DockerFile to create docker image

# Pre-requisites

Run docker, and cd into the cloned repository location

# Command to build Docker Image

docker build -t appdynamics:4.3.4.3 .

# Command to run Docker Image

docker run --name appdynamics -p 8090:8090 -t appdynamics:4.3.4.3

# Important Passwords to AppDynamics

-- Controller Root Access  
User: root  
Password: appdynamics  
  
-- Initial User Access  
User: admin  
Password: appdynamics  
  
-- Database  
Password: appdynamics  
  
Once the image is running, start the controller by using the following command:  
./bin/startController.sh
  
Access the Controller with the following URL:  
http://<docker-ip>:8090/controller